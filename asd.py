# Напишите функцию которая принемает *args
magazin = [
    {
    'tovar': 'chudo',
    'stoimost': 55,
    'prodaja': 75,
    'pribyl': 20
    },

    {
    'tovar': 'pechenie',
    'stoimost': 25,
    'prodaja': 50,
    'pribyl': 25
    },

    {
    'tovar': 'piko',
    'stoimost': 30,
    'prodaja': 40,
    'pribyl': 10

    }]

def prinimaet_args(*args):
    print(args)

prinimaet_args(magazin)

# Напишите функцию которая принемает *kwargs

def prinimaet_kwargs(**kwargs):
    print(kwargs)

prinimaet_kwargs(soki=25, pirojennoe=50, morojennoe=15)

# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
dota = ['shaker', 'slark', 'huskar']

def po_ciklu_args(*args):
    for po in args:
        for pi in po:
            print(pi)
po_ciklu_args(dota)

# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор),
# но с использованием параметров args, kwargs

telki = [
    {
        'name': 'jaka',
        'age': 25


    },
    {
        'name': 'masha',
        'age': 20
    },
    {
        'name': 'jibek',
        'age': 18
    }

]

rebyata = [
    {
        'name': 'nurs',
        'age': 25


    },
    {
        'name': 'erlan',
        'age': 20
    },
    {
        'name': 'maksat',
        'age': 18
    }

]

def v_clube(*args, **kwargs):
    for o in args:
        for peoples in o:
            if peoples['age'] >=20:
                print(kwargs["administrator"] + "Postavit pechat'" + "Vy podhodite nam\n")
            else:
                print(kwargs["GBR"] + peoples["name"] + "Ty idesh domoi spat'\n")

v_clube(rebyata, telki, GBR="MAMBET", administrator="Glamurnaya Ledi")