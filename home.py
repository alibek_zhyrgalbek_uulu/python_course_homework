# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать.
x = 15
y = 30

def vyvesti():
    summa = x + y
    print(summa)

vyvesti()



# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.
a = 21
b = 12
def raznost():
    minus = a - b
    print(minus)
raznost()

# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.

d = 14
c = 2
def proizved():
    umnoj = d * c
    print(umnoj)
proizved()

# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.

e = 24
g = 8
def delit():
    deli = e / g
    print(deli)
delit()


# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.

my_mass = [333, 777, 888]
def prohodit(vyvodit):
    print(vyvodit)


for u in my_mass:
    prohodit(u)
# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.

milli = ['alibek', 'jibek', 'malik']

def proitis(po_imenam):
    print(po_imenam)


for o in milli:
    proitis(o)
# Напишите приммеры использования всех операций с массивами
    # len()


chokolade = ['mars', 'sneakers', 'baunti', 'milka']
a = len(chokolade)
print(a)

    # append()

chokolade.append(78)
print(chokolade)

    # clear()

chokolade.clear()
print(chokolade)

    # count()
chokolade = ['mars', 'sneakers', 'baunti', 'milka', 'malik', 'malik']
k = chokolade.count('malik')
print(k)
    # copy()

cop = chokolade.copy()
print(cop)

    # extend()

cars = ['mers', 'aston_martin', 'ferrari']
chokolade.extend(cars)
print(chokolade)

    # index()

ind = cars.index('ferrari')
print(ind)

    # remove('Meder')

cars.remove('aston_martin')
print(cars)



    # reverse()

cars.reverse()
print(cars)
    # pop()

cars.pop()
print(cars)

# Оберните все операции в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.
    # len()

y = ['yabloko', 'mandarin', 'vinograd', 'vishnya']
def magazin(jungle):
    ji = len(jungle)
    print(ji)
    jungle.append('ananas')
    print(jungle)
magazin(y)


    # append()

def dobav(frukt):
    frukt.append('ananas')
    print(frukt)
dobav(y)


    # clear()
def udali(frukty):
    frukty.clear()
    print(frukty)
udali(y)

    # count()
y = ['yabloko', 'mandarin', 'vinograd', 'vishnya', 'vishnya', 'vishnya']
def skolko(vishen):
    m = vishen.count('vishnya')
    print(m)
skolko(y)

    # copy()

def kopir(mass):
    mass.copy()
    print(mass)
kopir(y)
    # extend()

def dobavte(massivy):
    m = [20, 30, 40, 50, 60]
    massivy.extend(m)
    print(massivy)
dobavte(y)
    # index()

def index_massiva(pokaji):
    v = pokaji.index('vinograd')
    print(v)
index_massiva(y)
# remove()
def udali_kakoito_frukt(citrus):
    citrus.remove('mandarin')
    print(citrus)

udali_kakoito_frukt(y)


    # reverse()

def pomenyai_mestami(znacheniya):
    znacheniya.reverse()
    print(znacheniya)
pomenyai_mestami(y)


    # pop()

def udali_posledniy_element(iz_massiva):
    iz_massiva.pop()
    print(iz_massiva)
udali_posledniy_element(y)