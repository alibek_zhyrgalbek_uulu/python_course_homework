# Напишите приммеры использования всех операций со словарями
    # clear()

slova = {'bum', 'cum', 'doom'}
slova.clear()
print(slova)

    # copy()
slova = {'bum': 30, 'cum': 200, 'doom': 300}
povtor = slova.copy()
print('----original---', slova)
print('----new---', povtor)




#1 пример


klychi = {'a', 'b', 'v', 'g', 'd'}

alphabet = dict.fromkeys(klychi)
print('-----alfavit-----', alphabet)

#2 пример


klychi = {'p', 'b', 's', 'z', 'd', 't'}
paired = 'consonant'
consonant = dict.fromkeys(klychi, paired)
print(consonant)

#3 пример
klychi = {'p', 'b', 's', 'z', 'd', 't'}
paired = [1]

consonant = dict.fromkeys(klychi, paired)
print(consonant)

paired.append(2)
print(consonant)

     # get()
friend = {'name': 'Erjigit', 'age': 25}

print('Name: ', friend.get('name'))
print('Age: ', friend.get('age'))

# value is not provided
print('zarobabotnaya plata: ', friend.get('zarabotal'))

# value is provided
print('zarabotnaya plata: ', friend.get('zarabotal', 1000))



    # items()

magazin = {'fanta': 5, 'piko': 10, 'fuise tea': 15}
print(magazin.items())

magazin = {'fanta': 5, 'piko': 10, 'fuise tea': 15}
items = magazin.items()
print('naturalnye soki:', items)

del[magazin['fanta']]
print('obnovlenie items:', items)


    # keys()

drug = {'name': 'Malik', 'age': 25, 'salary': 1200}
print(drug.keys())

slovar = {}
print(slovar.keys())

drugan = {'name': 'Meder', 'age': 22, }

print('Before dictionary is updated')
keys = drugan.keys()
print(keys)

drugan.update({'salary': 99999})
print('\nAfter dictionary is updated')
print(keys)

    # values()


magazinchik = {'tort': 2, 'pirog': 3, 'pryaniki': 4}

print(magazinchik.values())

magazinchik = {'tort': 2, 'pirog': 3, 'pryaniki': 4}

values = magazinchik.values()
print('originalnye:', values)


del[magazinchik['tort']]
print('obnovlennye:', values)
    # pop()

#1 пример
magazinchik = {'tort': 2, 'pirog': 3, 'pryaniki': 4}

mumu = magazinchik.pop('tort')
print('udalenniy:', mumu)
print('obnovlennyi:', magazinchik)


    # popitem()

bratiwka = {'name': 'Erjan', 'age': 22, 'dengi': 1500 }

result = bratiwka.popitem()

print('zarabatok= ', result)
print('bratiwka = ', bratiwka)


bratiwka['zanimaetsya'] = 'kachkoi'


result = bratiwka.popitem()

print('Return Value = ', result)
print('person = ', bratiwka)



# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.

my_friend = {
    'name': 'Berkut',
    'year_o': 27,
    'profession': 'admin'
}


def dict_functions(any_dict):
    # copy()
    my_friend_only = any_dict.copy()
    print(f'-----copy-----{my_friend_only}')

    # fromkeys()
    keys = {'2', '3', '5', '7'}
    value = 'prime number'
    prime_numbers = dict.fromkeys(keys, value)
    print(f'-----fromkeys-----{prime_numbers}')

    # get()
    print(f'-----get-----{my_friend.get("name")}')
    print(f"-----get-----{my_friend.get('volume', 'There is no such keys')}")

    # keys()
    print(f"-----keys----{any_dict.keys()}")

    # values()
    print(f"-----values-----{any_dict.values()}")

    # items()
    print(f"----items----{any_dict.items()}")

    for keys, values in any_dict.items():
        print(f"----item with unpacking-----{keys, values}")

    # pop()
    print(f"----original---{any_dict}")
    element = any_dict.pop('year_o')
    print(f"-----popped element----{element}")
    print(f"-----after popping---{any_dict}")

    item = any_dict.pop('id', 'Takih klychei net')
    print(f"-----pop-----{item}")

    # popitem()
    popped_element = any_dict.popitem()
    print(f"-----popitem----{popped_element}")

    # clear()
    any_dict.clear()
    print(f"-----clear----{any_dict}")


dict_functions(my_friend)

# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()
# --- > Здесь опишите решение
# dict.update()
electro_car = {
    'name': 'tesla',
    'km': 300
}

electro = {
    'km': 320
}

print(f"original -->{electro_car}")
electro_car.update(electro)

print(f"posle obnovy --> {electro_car}")

electro = {
    'max_km': 100000000
}

electro_car.update(electro)
print(f"obnova s new element --> {electro_car}")

# dict.setdefault()

car_max_km = electro_car.setdefault('max_km')

print(f"----setdefault--- {car_max_km}")

maximum_km = electro_car.setdefault('maksimum')

print(f"---setdefault---- {maximum_km}")

opredelit = electro_car.setdefault('opredelit_stoimost_mashiny', 300000)

print(f"---setdefault--- {opredelit}")
print(electro_car)


# Напишите пример вложенной функции.
# --- > Здесь опишите решение
def kalkulyator(a, b):
    def pribavit():
        print(a + b)

    return pribavit()


kalkulyator(12, 12)

# Напишите функцию принимающую массив (состоящий из слов, название файла)
# и при помощи данных аргументов дополняющую файл новыми записями

numbers = [76, 67, 89, 234]


def adding_to_file(nums):
    file = open('words', 'a')
    for num in nums:
        file.write(str(num) + '\n')


adding_to_file(numbers)


# Напишите функцию считывающую данные из файла


def reading_file():
    file = open('words', 'r')
    print(file.readlines())


reading_file()
# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает только отфильтрованные данные
numb = [44, 54, 13, 43, 2, 15, 22, 100]


def filter(int):
    file = open('even_numbers', 'a')
    for i in int:
        if i % 2 == 0:
            file.write(str(i) + '\n')


filter(numbers)
